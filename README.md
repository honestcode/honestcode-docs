# HonestCode general documentation

HonestCode is an Open Source SASS platform for BDD/ATDD

You can get more information at [HonestCode.io](https://www.HonestCode.io) or try for free at [pro.HonestCode.io](https://pro.HonestCode.io) 

This repository includes the global documentation. This repository is not required to run this project

![HonestCode Schema](/images/HonestCode-Architecture.png)
This schema can be found [here](https://drive.google.com/file/d/1SC_PmElBCe_S3q43_ZEjq-IOmGlWY4qe/edit).

This project includes the following repositories

```
HonestCode
│   nginx
│   www
│   * docs (* not required) 
│
└───API Backend
   │   hc-api-proxy
   │   hc-api-private
   │
   └───Microservices
   │   │   hc-profiles
   │   │   hc-organizations (Teams)
   │   │   hc-projects (Products)
   │   │   hc-blueprints
   │   │   hc-features
   │   │   hc-scenarios
   │   │   hc-steps
   │      
   └───Aux
       │   hc-workers
       │   * hc-infrastructure (* not required)
```

Here are some useful resources:

  * [HonestCode web](https://honestcode.io) info, blog and FAQs about HonestCode
  * [HonestCode service](https://pro.honestcode.io) fully working and ready to use web service
  * [HonestCode repository](https://gitlab.com/honestcode) the open source repositories


## Feedback & Questions

Share feedback and ask questions on the HonestCode [Gitter Channel](https://gitter.im/honest-code)

For any other questions or feedback, please email to [honestcode@intelygenz.com](honestcode@intelygenz.com) / <honestcode@intelygenz.com>


## Contributing

Learn how to [Contributing.md](https://gitlab.com/honestcode/honestcode-docs/blob/master/CONTRIBUTING.md)


## Code of conduct

Read our [Code_of_Conduct.md](https://gitlab.com/honestcode/honestcode-docs/blob/master/CODE_OF_CONDUCT.md)


## License
   Copyright 2020 Intelygenz Inc. & Intelygenz S.A.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

